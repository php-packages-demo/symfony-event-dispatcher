# symfony/event-dispatcher

Dispatching events and listening to them https://symfony.com/event-dispatcher

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-filesystem+php-symfony-console+php-symfony-process+php-symfony-finder+php-symfony-expression-language+php-symfony-cache+php-symfony-config+php-symfony-dependency-injection+php-symfony-event-dispatcher&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

## Documented design patterns
* From: [*The EventDispatcher Component*](https://symfony.com/doc/current/components/event_dispatcher.html)
* See: [Programming patterns and anti-patterns](https://gitlab.com/notes-on-computer-programming-languages/notes-on-computer-programming-languages/-/blob/master/README.md#programming-patterns-and-anti-patterns)
### Mediator
### Observer
* (Functional reactive programming (FRP) in functional programming)

## EDA (Event-Driven Architecture) unofficial design pattern
* [*Unlock the Secrets of Symfony’s Kernel Events: What Every Developer Must Know!*
  ](https://medium.com/@skowron.dev/unlock-the-secrets-of-symfonys-kernel-events-what-every-developer-must-know-7ec39f3fc003)
  2024-05 Jakub Skowron (skowron.dev, Medium)
  * But Symfony is itself a call-back for the event-driven webserver!

## Symfony official documentation
* [*The EventDispatcher Component*](https://symfony.com/doc/current/components/event_dispatcher.html)
* [*Create Framework: The EventDispatcher Component*
  ](https://symfony.com/doc/current/create_framework/event_dispatcher.html)
* [*The Container Aware Event Dispatcher (4.0+)*](https://symfony.com/doc/current/components/event_dispatcher/container_aware_dispatcher.html)
  * The ContainerAwareEventDispatcher was removed in Symfony 4.0. Use EventDispatcher with closure-proxy injection instead.
  * [closure-proxy injection](https://www.google.com/search?q=closure-proxy+injection)
  * [*Proxy Injection*](https://medium.com/@assertchris/proxy-injection-17f2e2b01cb8) 2014
  * [*Dependency Injection slowness solved by Doctrine Proxies, Dependency Injection Containers and Performance*](https://ocramius.github.io/blog/zf2-and-symfony-service-proxies-with-doctrine-proxies/)
* [*The Container Aware Event Dispatcher (<= 3.4)*](https://symfony.com/doc/3.4/components/event_dispatcher/container_aware_dispatcher.html)
* [*New in Symfony 4.4: Simpler Event Listeners*
  ](https://symfony.com/blog/new-in-symfony-4-4-simpler-event-listeners)
  2019-10 Javier Eguiluz

## Contributed documentation
* (fr) [*Comprendre Symfony avec l'EventDispatcher*
  ](https://afsy.fr/avent/2019/14-comprendre-symfony-avec-eventdispatcher)
  2019-12 Rémi Andrieux
* [*Decouple your Symfony application using Domain Events*
  ](https://romaricdrigon.github.io/2019/08/09/domain-events)
  2019-08 Romaric Drigon

### Listeners controversy
* [*How to disable the Listeners in Symfony*
  ](https://medium.com/@smaine.milianni/how-to-disable-the-listeners-in-symfony-3d30de1d6929)
  2019-10 Smaine Milianni
* [*How to Convert Listeners to Subscribers and Reduce your Configs*
  ](https://www.tomasvotruba.cz/blog/2019/07/22/how-to-convert-listeners-to-subscribers-and-reduce-your-configs/)
  2019-07 Tomas Votruba
* [*Don't Ever use Symfony Listeners*
  ](https://www.tomasvotruba.cz/blog/2019/05/16/don-t-ever-use-listeners/)
  2019-05 Tomas Votruba
