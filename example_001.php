#!/usr/bin/env php
<?php

require_once "vendor/autoload.php";

use Symfony\Component\EventDispatcher\EventDispatcher;

$dispatcher = new EventDispatcher();