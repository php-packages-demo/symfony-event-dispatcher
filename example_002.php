#!/usr/bin/env php
<?php

require_once "vendor/autoload.php";

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Contracts\EventDispatcher\Event;

class StoreEvents {

    /**
    * @Event("Symfony\Contracts\EventDispatcher\Event")
    */
    public const ACME_FOO_ACTION = 'acme.foo.action';
}

$dispatcher = new EventDispatcher();
$dispatcher->addListener(
    StoreEvents::ACME_FOO_ACTION,
    function (Event $event): void {
        // will be executed when the acme.foo.action event is dispatched
        echo StoreEvents::ACME_FOO_ACTION . PHP_EOL;
    }
);
$dispatcher->dispatch(
    new Event(),
    StoreEvents::ACME_FOO_ACTION
);