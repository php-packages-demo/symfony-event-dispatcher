#!/usr/bin/env php
<?php

require_once "vendor/autoload.php";

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Contracts\EventDispatcher\Event;

class StoreEvents {

    /**
    * @Event("Symfony\Contracts\EventDispatcher\Event")
    */
    public const ACME_FOO_ACTION = 'acme.foo.action';
}

class AcmeListener
{
    // ...

    public function onFooAction(Event $event): void
    {
        // ... do something
        echo 'onFooAction' . PHP_EOL;
        echo StoreEvents::ACME_FOO_ACTION . PHP_EOL;
    }
}

$dispatcher = new EventDispatcher();
$dispatcher->addListener(
    StoreEvents::ACME_FOO_ACTION,
    [
        new AcmeListener(),
        'onFooAction'
    ]
);
$dispatcher->dispatch(
    new Event(),
    StoreEvents::ACME_FOO_ACTION
);
