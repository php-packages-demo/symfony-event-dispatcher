#!/usr/bin/env php
<?php

require_once "vendor/autoload.php";

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Contracts\EventDispatcher\Event;

class StoreEvents {

    /**
    * @Event("Symfony\Contracts\EventDispatcher\Event")
    */
    public const ACME_FOO_ACTION = 'acme.foo.action';
}

final class AcmeFooEvent extends Event
{
    public function trace(): void {
        echo 'From AcmeFooEvent' . PHP_EOL;
    }
}

$dispatcher = new EventDispatcher();
$dispatcher->addListener(
    StoreEvents::ACME_FOO_ACTION,
    function (AcmeFooEvent $event): void {
        // will be executed when the acme.foo.action event is dispatched
        // $event-­>trace();
        echo 'from listener' . PHP_EOL;
    }
);
$dispatcher->dispatch(
    new AcmeFooEvent(),
    StoreEvents::ACME_FOO_ACTION
);